package com.example.dan.weatherapp;

import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class MainActivity extends ActionBarActivity {
//what will be run on launch
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LocationManager findlocation = (LocationManager) getSystemService(LOCATION_SERVICE);

        //if gps is not enabled, enable in settings.
        if (!findlocation.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        } else {
            try {
                Criteria criteria = new Criteria();
                String provider = findlocation.getBestProvider(criteria, false);
                Location loc = findlocation.getLastKnownLocation(provider);
                new Weather().execute(loc.getLatitude(), loc.getLongitude());
            } catch (Exception e) {

            }

        }
    }

        class Weather extends AsyncTask<Double, Double, Integer> {

            String desc, locationd;
            Double temp;
//parses json data 
            @Override
            protected Integer doInBackground(Double... longlat) {
                HttpClient client = new DefaultHttpClient();
                HttpResponse response;
                String responseString;
                try {

                    String apiurl = "http://api.wunderground.com/api/b2f692b7a415be7d/conditions/q/" + longlat[0] + "," + longlat[1] + ".json";
                    response = client.execute(new HttpGet(apiurl));
                    StatusLine statusLine = response.getStatusLine();
                    if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        response.getEntity().writeTo(out);
                        responseString = out.toString();


                        JSONObject json = new JSONObject(responseString);



                            JSONObject weatherDetails = new JSONObject(json.getString("current_observation")); //Weather details such as description, icon etc...
                            JSONObject countryDetails = new JSONObject(weatherDetails.getString("observation_location"));
                            temp = weatherDetails.getDouble("temp_c");
                            desc = weatherDetails.getString("weather");
                            locationd = countryDetails.getString("full");

                    }
                } catch (JSONException | IOException e) {
                  e.printStackTrace();
                }
                return 1;
            }

            @Override
            protected void onPostExecute(Integer integer) {
                TextView temp1 = (TextView) findViewById(R.id.txtTemp);
                TextView desc1 = (TextView) findViewById(R.id.txtDesc);
                TextView country1 = (TextView) findViewById(R.id.txtLocation);

                temp1.setText(Math.round(temp.intValue()) + "°Celsius");
                desc1.setText(desc);
                country1.setText(locationd);

            }
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }}

